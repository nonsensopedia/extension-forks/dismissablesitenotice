( function () {
	var cookieName = 'dismissSiteNotice',
		siteNoticeId = mw.config.get( 'wgSiteNoticeId' );

	// If no siteNoticeId is set, exit.
	if ( !siteNoticeId ) {
		return;
	}

	// If the user has the notice dismissal cookie set, exit.
	if ( $.cookie( cookieName ) === siteNoticeId ) {
		return;
	}

	// Enable the dismiss button.
	$( function () {
		// eslint-disable-next-line no-jquery/no-global-selector
		$( '.mw-dismissable-notice-close' )
			.find( 'a' )
			.on( 'click keypress', function ( e ) {
				if (
					e.type === 'click' ||
					e.type === 'keypress' && e.which === 13
				) {
					$( this ).closest( '.mw-dismissable-notice' )
						.removeClass( 'mw-dismissable-notice-show' );
					$.cookie( cookieName, siteNoticeId, {
						expires: 30,
						path: '/'
					} );
				}
			} );
	} );

}() );
