<?php

class DismissableSiteNoticeHooks {

	/**
	 * @param string &$notice
	 * @param Skin $skin
	 */
	public static function onSiteNoticeAfter( string &$notice, Skin $skin ) {
		global $wgMajorSiteNoticeID, $wgDismissableSiteNoticeForAnons;

		if ( !$notice ) {
			return;
		}

		// Dismissal for anons is configurable
		if ( !$wgDismissableSiteNoticeForAnons && !$skin->getUser()->isRegistered() ) {
			return;
		}

		// Cookie value consists of two parts
		$major = (int)$wgMajorSiteNoticeID;
		$minor = (int)$skin->msg( 'sitenotice_id' )->inContentLanguage()->text();
		$siteNoticeID = "$major.$minor";

		$out = $skin->getOutput();
		$out->addModuleStyles( 'ext.dismissableSiteNotice.styles' );
		$out->addModules( 'ext.dismissableSiteNotice' );
		$out->addJsConfigVars( 'wgSiteNoticeId', $siteNoticeID );

		$notice = Html::rawElement(
			'div',
			[
				'id' => 'mw-dismissable-notice',
				'class' => 'mw-dismissable-notice',
				'data-nosnippet' => ''
			],
			Html::rawElement(
				'div',
				[ 'class' => 'mw-dismissable-notice-close' ],
				$skin->msg( 'sitenotice_close-brackets' )
					->rawParams(
						Html::element(
							'a',
							[
								'tabindex' => '0',
								'role' => 'button',
								'style' => 'cursor: pointer;'
							],
							$skin->msg( 'sitenotice_close' )->text()
						)
					)
					->escaped()
			) .
			Html::rawElement( 'div', [ 'class' => 'mw-dismissable-notice-body' ], $notice )
		);

		// Yeah, this is ugly as all hell, but this is the only way to show/hide the notice based
		// on cookies without the page jumping around when loading. A server-side solution is
		// possible, but it would break HTML caching.
		$notice .= Html::inlineScript( <<<JS
(function(){
if(document.cookie.indexOf('dismissSiteNotice=$siteNoticeID')!==-1)return;
var node=document.getElementById('mw-dismissable-notice');
if(node){node.className+=' mw-dismissable-notice-show';}
}());
JS
		);
	}
}
